{-# LANGUAGE PackageImports #-}
import "pemfung" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
