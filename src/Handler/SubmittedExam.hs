{-# LANGUAGE OverloadedStrings #-}
module Handler.SubmittedExam where
import Import
import Database.Persist.Postgresql

postSubmittedExamR :: Handler Value
postSubmittedExamR = do
    post <- requireJsonBody :: Handler SubmittedExam
    pid <- runDB $ insert post
    sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getSubmittedExamR :: Handler Value
getSubmittedExamR = do
    dt <- runDB $ selectList [] [Asc SubmittedExamId]
    sendStatusJSON ok200 (object ["content" .= dt])

getSubmittedExamSpecificR :: SubmittedExamId -> Handler Value
getSubmittedExamSpecificR idPost = do
    dt <- runDB $ get404 idPost
    sendStatusJSON ok200 (object ["content" .= dt])

deleteSubmittedExamSpecificR :: SubmittedExamId -> Handler Value
deleteSubmittedExamSpecificR idPost = do
    _ <- runDB $ get404 idPost
    runDB $ deleteCascade idPost
    sendStatusJSON noContent204 (object [])


optionsSubmittedExamSpecificR :: SubmittedExamId -> Handler RepPlain
optionsSubmittedExamSpecificR idPost = do
    return $ RepPlain $ toContent ("" :: Text)

optionsSubmittedExamR :: Handler RepPlain
optionsSubmittedExamR = do
    return $ RepPlain $ toContent ("" :: Text)