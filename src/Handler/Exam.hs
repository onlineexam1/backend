{-# LANGUAGE OverloadedStrings #-}
module Handler.Exam where
import Import
import Database.Persist.Postgresql

postExamR :: Handler Value
postExamR = do
    post <- requireJsonBody :: Handler Exam
    pid <- runDB $ insert post
    sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getExamR :: Handler Value
getExamR = do
    dt <- runDB $ selectList [] [Asc ExamId]
    sendStatusJSON ok200 (object ["content" .= dt])

optionsExamR :: Handler RepPlain
optionsExamR = do
    return $ RepPlain $ toContent ("" :: Text)

getExamSpecificR :: ExamId -> Handler Value
getExamSpecificR idPost = do
    dt <- runDB $ get404 idPost
    sendStatusJSON ok200 (object ["content" .= dt])

putExamSpecificR :: ExamId -> Handler Value
putExamSpecificR idPost = do
    _ <- runDB $ get404 idPost
    newPost <- requireJsonBody :: Handler Exam
    runDB $ replace idPost newPost
    sendStatusJSON noContent204 (object [])

deleteExamSpecificR :: ExamId -> Handler Value
deleteExamSpecificR idPost = do
    _ <- runDB $ get404 idPost
    runDB $ deleteCascade idPost
    sendStatusJSON noContent204 (object [])

optionsExamSpecificR :: ExamId -> Handler RepPlain
optionsExamSpecificR idPost = do
    return $ RepPlain $ toContent ("" :: Text)
