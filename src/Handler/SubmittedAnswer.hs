{-# LANGUAGE OverloadedStrings #-}
module Handler.SubmittedAnswer where
import Import
import Database.Persist.Postgresql

postSubmittedAnswerR :: Handler Value
postSubmittedAnswerR = do
    post <- requireJsonBody :: Handler SubmittedAnswer
    pid <- runDB $ insert post
    sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getSubmittedAnswerR :: Handler Value
getSubmittedAnswerR = do
    dt <- runDB $ selectList [] [Asc SubmittedAnswerId]
    sendStatusJSON ok200 (object ["content" .= dt])

getSubmittedAnswerWithSubmittedQuestionIdR :: SubmittedExamId -> Handler RepPlain
getSubmittedAnswerWithSubmittedQuestionIdR idPost = do
    dt <- runDB $ selectList [SubmittedAnswerSubmittedExamId <-. [idPost]] [] 
    sendStatusJSON ok200 (object ["content" .= dt])


optionsSubmittedAnswerWithSubmittedQuestionIdR :: SubmittedExamId -> Handler RepPlain
optionsSubmittedAnswerWithSubmittedQuestionIdR idPost = do
    return $ RepPlain $ toContent ("" :: Text)
    
optionsSubmittedAnswerR :: Handler RepPlain
optionsSubmittedAnswerR = do
    return $ RepPlain $ toContent ("" :: Text)