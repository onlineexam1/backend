{-# LANGUAGE OverloadedStrings #-}
module Handler.Question where
import Import
import Database.Persist.Postgresql

postQuestionR :: Handler Value
postQuestionR = do
    post <- requireJsonBody :: Handler Question
    pid <- runDB $ insert post
    sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getQuestionR :: Handler Value
getQuestionR = do
    dt <- runDB $ selectList [] [Asc QuestionId]
    sendStatusJSON ok200 (object ["content" .= dt])

optionsQuestionR :: Handler RepPlain
optionsQuestionR = do
    return $ RepPlain $ toContent ("" :: Text)

getQuestionWithExamIdR :: ExamId -> Handler Value
getQuestionWithExamIdR idPost = do
    dt <- runDB $ selectList [QuestionExamId <-. [idPost]] [] 
    sendStatusJSON ok200 (object ["content" .= dt])

optionsQuestionWithExamIdR :: ExamId -> Handler RepPlain
optionsQuestionWithExamIdR idPost = do
    return $ RepPlain $ toContent ("" :: Text)

getQuestionSpecificR :: QuestionId -> Handler Value
getQuestionSpecificR idPost = do
    dt <- runDB $ get404 idPost
    sendStatusJSON ok200 (object ["content" .= dt, "id" .= idPost])

putQuestionSpecificR :: QuestionId -> Handler Value
putQuestionSpecificR idPost = do
    _ <- runDB $ get404 idPost
    newPost <- requireJsonBody :: Handler Question
    runDB $ replace idPost newPost
    sendStatusJSON noContent204 (object [])

deleteQuestionSpecificR :: QuestionId -> Handler Value
deleteQuestionSpecificR idPost = do
    _ <- runDB $ get404 idPost
    runDB $ deleteCascade idPost
    sendStatusJSON noContent204 (object [])


optionsQuestionSpecificR :: QuestionId -> Handler RepPlain
optionsQuestionSpecificR idPost = do
    return $ RepPlain $ toContent ("" :: Text)
